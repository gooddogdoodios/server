const _ = require('lodash');

function gameState() {

    return function gameState(players) {

        let resources = players.reduce((acc, curr, i, arr) => {
            acc[curr.id] =  {
                energy: {
                    value: 100,
                    increment: 5,
                },
            };
            return acc;
        }, {});

        /**
         * @param {Object} update - contains the resources to update and their respectives values and modifiers
         */
        function updateState(u) {
            let s = resources;
            Object.keys(u).forEach(p => Object.keys(u[p]).forEach((k,v) => s[p][k] = u[p][k]));
        }

        /**
         * @param {number} delta time since last update
         **/
        function updateStateWithTime(delta) {
            let s = resources;
            Object.keys(s).forEach(p => Object.keys(s[p]).forEach(r => {
                s[p][r].value = s[p][r].value + (delta/1000 * s[p][r].increment);
            })); 
        }

        function getState() { 
            return resources;
        }

        return {
            getState,
            updateState,
            updateStateWithTime,
        };

    };
}

module.exports = gameState();
