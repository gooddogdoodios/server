/**
 * @author Ibrahim Rabbani
 *
 * @name match
 *
 * @description a match object contains methods for updating the state
 * of the match, the end conditions, and details about the player
 *
 * @param {Object[]} players - the players participating in the match
 * @param {string} players.id - unique identifier for the player's session
 * @param {string} players.login - user name of the player
 * @param {Object} players.socket - the socket object (event emitter) for the player
 *
 * @returns {Object} match object
 */
function match(configuration, players) {
    
    let id = players.map(p => p.id).join('');

    let players = players;

    let playerStates = players.reduce((acc, curr, i, arr) =>  {
        acc[curr.id] = _getInitialGameState;
        return acc;
    }, {});

    function getState() { return playerStates }

    /**
     * @todo: add documentation once there's a more complete definition
     */
    function updateState(changes, state) {

        // update the state

        state.

    }

    /**
     * @param {Object} state
     */
    function _checkEndConditions(state) {
        // for now the game ends if one player runs out of energy
        return state
            .map(p => p.resources
                .reduce((acc, curr) => acc === false ? acc : curr.value === 0))
            .reduce((acc, curr) => acc === false ? acc : curr === false );
    }

    /**
     * @todo: add documentation once there's a more complete definition
     */
    function _getInitialGameState() {
        return {
            energy: {
                value: 100,
                delta: 0.10
            }
        };
    }



    return {
        getState,
        updateState
    };

}
