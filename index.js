let express = require('express'),
    _ = require('lodash'),
    socketIO = require('socket.io'),
    gameState = require('./gameState');

const PORT = process.env.PORT || 8080,
      HOST = process.env.HOST || '0.0.0.0';

let app = express()
    .use((req, res) => res.send("Hello world"))
    .listen(PORT, HOST, () => console.log(`Listening @ ${ HOST } : ${ PORT }`));

const io = socketIO(app);

// matchmaking queue for players
let queue = [];

/**
 * Matches that are currently in process
 * The matchid is the key and the value is the state of the match
 */
let matches = {}; 

io.on('connection', _handleConnection);

function _handleConnection(socket) {
    console.log('Player connected');
    socket.emit('socketID', { id: socket.id });
    socket.on('login', _handleLogin(socket));
    socket.on('disconnect', _handleDisconnect(socket));
    socket.on('changeState', _handleChangeState);
}

function _handleLogin(socket) {

    return (args) => {

        let player = {
            id: socket.id,
            login: args.login,
            socket
        };

        queue.push(player);

        console.log(`User ${ player.login } logged in successfully`);
        console.log(`User ${ player.login } added to matchmaking queue`);

        if (queue.length > 1) {

            let players = queue.splice(0,2),
                matchId = players.map(p => p.id).join(''),
                match = {
                    state: new gameState(players, matchId),
                    players: players.reduce((acc, curr, i, arr) => {
                        acc[curr.id] = arr[i];
                        return acc;
                    }, {})
                };

            matches[matchId] = match;

            players.forEach(p => {
                p.socket.emit('matchFound', {
                    id: p.id,
                    state: match.state.getState(),
                    opponent: players.find(opp => opp.id !== p.id).login,
                    opponentId: players.find(opp => opp.id !== p.id).id,
                    matchId
                });
            });

            setInterval(_update(match, matchId), 1000, 1000);

            console.log(`Match created: ${ players[0].login } vs ${ players[1].login } successfully`);
        }
    }

}

/**
 * Create a timer to update each match
 **/
function _update(match, matchId) {
    return (time) => {
        match.state.updateStateWithTime(time);
        _.forEach(match.players, p =>  p.socket.emit('stateUpdate', {
            id: p.id,
            state: match.state.getState(),
            opponent: _.find(match.players, (v, k) => v.id !== p.id).login,
            opponentId: _.find(match.players, (v, k) => v.id !== p.id).id,
            matchId: matchId
        }));
        console.log('Update', match.state.getState());
    }
}

function _handleChangeState(payload) {
    let match = matches[payload.matchId];
    let state = _.pickBy(payload, (val, key) => key !== 'matchId');
    state = _.mapValues(state, (val, key, obj) => JSON.parse(val));
    match.state.updateState(state);
    _.forEach(match.players, p =>  p.socket.emit('stateUpdate', {
        id: p.id,
        state: match.state.getState(),
        opponent: _.find(match.players, (v, k) => v.id !== p.id).login,
        opponentId: _.find(match.players, (v, k) => v.id !== p.id).id,
        matchId: payload.matchId
    }));
}

function _handleDisconnect(socket) {
    return () => {
        queue = queue.filter(p => socket.id === p.id ? false : true);
        console.log('Player disconnected');
    }
}
